import Vue from 'vue'
import SvgIcon from '@/components/SvgIcon'// svg component

// register globally
// 1.全局注册svg-icon组件
Vue.component('svg-icon', SvgIcon)

// 2.引入目录下所有svg
// require.context(目标目录【路径】，是否扫描子目录，正则表达式【凡事.svg结尾的文件都要】)扫描目录中的文件
const req = require.context('./svg', false, /\.svg$/)
console.log(req.keys())
const requireAll = requireContext => requireContext.keys().map(requireContext)
// 数组方法map foreach都是循环数组
// map是循环每一项 svg图片map(() =》 {})
// req函数能够引用图片到项目中
// 将所有的svg都引用到项目中
requireAll(req)// 调用函数

// 3、loader插件打包svg--svg-sprite-loader打包了所有svg到一个svg标签上，将svg名称作为symbol标签的id属性
// webpack loader

// 4.svg-icon引用svg链接--svg-icon使用iconClass属性引用了symbol的id

