import axios from 'axios'
import store from '@/store'
import { Message } from 'element-ui'
// axios.create({})--创建一个新的axios实例
const service = axios.create({
  // 设置基地址和超时时间
  // baseURL:'/api'
  baseURL: 'process.env.VUE_APP_BASE_API', // 基地址
  timeout: 10000// 超时时间
})

// 【请求拦截器-interceptors.request】
// interceptors.request.use--拦截器的请求拦截使用。
// 进行请求操作时，使用特定的拦截器来处理请求，并在请求发送之前执行一些自定义逻辑
// 两个回调函数---成功执行1，失败执行2
service.interceptors.request.use((config) => {
  // 请求接口 config是请求配置
  // 注入token
// store.getters.token => 请求头里面
  if (store.getters.token) {
    // 只要有token就要检查token的时效性
    // 如果存在token
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }
  // 注意一定要return config
  return config
}, (error) => {
  // 失败执行promise--终止promise的执行
  return Promise.reject(error)
})

// 【响应拦截器-interceptors.response】
service.interceptors.response.use((response) => {
  // 成功执行promise
  // axios默认包裹了一层data
  const { data, message, success } = response.data
  if (success) {
    // 此时认为业务执行成功了
    // 返回用户需要的数据data
    return data
  } else {
    // 此时认为业务执行失败的时候
    //  提示信息
    Message({ type: 'error', message })
    return Promise.reject(new Error(message))
  }
}, (error) => {
  // error.message提示错误
  Message({ type: 'error', message: error.message })
  return Promise.reject(error)
})

export default service
