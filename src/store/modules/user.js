import { getToken, setToken, removeToken } from '@/utils/auth'

// 存储数据
const state = {
  // getToken()获取token
  token: getToken() // 从缓存中读取初始值
}
// 修改数据
const mutations = {
  setToken(state, token) {
    state.token = token

    // setToken()设置token
    setToken(token) // 同步到缓存
  },
  removeToken() {
    // 删除Vuex的token
    state.token = null
    removeToken()
  }
}
// 做异步操作
const actions = {
  // context上下文，传入数据
  login(context, data) {
    console.log(data)
    // todo:调用登录接口
    // 返回一个token 123456
    context.commit('setToken', '123456')
  }
}

// 默认导出
export default {
  namespaced: true, // 开启命名空间
  state,
  mutations,
  actions
}
